package inverted

import java.io.File

import org.scalatest.FunSuite

class IndexTest extends FunSuite{

  import InvertedIndex.sc

  test("dictionary0"){
    val directory = new File("src/test/resources/dictionary0")
    val dictionary: Dictionary = Dictionary.fromDirectory(sc, directory)
    val index: Index = Index.fromDirectory(sc, dictionary, directory)

    val annaId = dictionary("anna")
    val bertaId = dictionary("berta")
    val claraId = dictionary("clara")
    val doraId = dictionary("dora")

    assert( index.documents(annaId) === Seq(0) )
    assert( index.documents(bertaId) === Seq(0) )
    assert( index.documents(claraId) === Seq(0) )
    assert( index.documents(doraId) === Seq(1) )

    assert( index.words(0) === Seq(annaId,bertaId,claraId) )
    assert( index.words(1) === Seq(doraId) )
  }

  test("dictionary0 without explicit dictionary"){
    val directory = new File("src/test/resources/dictionary0")
    val index: Index = Index.fromDirectory(sc, directory)
    val dictionary: Dictionary = index.dictionary

    val annaId = dictionary("anna")
    val bertaId = dictionary("berta")
    val claraId = dictionary("clara")
    val doraId = dictionary("dora")

    assert( index.documents(annaId) === Seq(0) )
    assert( index.documents(bertaId) === Seq(0) )
    assert( index.documents(claraId) === Seq(0) )
    assert( index.documents(doraId) === Seq(1) )

    assert( index.words(0) === Seq(annaId,bertaId,claraId) )
    assert( index.words(1) === Seq(doraId) )

    assert( index.documents("anna") === Seq(0) )
    assert( index.documents("berta") === Seq(0) )
    assert( index.documents("clara") === Seq(0) )
    assert( index.documents("dora") === Seq(1) )
  }

  test("datasets"){
    val directory = new File("src/main/resources/datasets")
    val dictionary: Dictionary = Dictionary.fromDirectory(sc, directory)
    val index: Index = Index.fromDirectory(sc, dictionary, directory)

    val shakespeareId = dictionary("Shakespeare")
    val shakespeares = index.documents(shakespeareId).toSet
    assert( shakespeares.contains(0) )
    assert( shakespeares === Set(0, 1, 2, 3, 7, 12, 15, 19, 20, 30, 33, 34, 39, 44) )
  }

}
