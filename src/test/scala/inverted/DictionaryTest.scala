package inverted

import java.io.File

import org.scalatest.FunSuite

class DictionaryTest extends FunSuite {

  import InvertedIndex.sc

  test("dictionary0"){
    val d0 = new File("src/test/resources/dictionary0")
    val d: Dictionary = Dictionary.fromDirectory(sc, d0)

    assert(d.size === 4)
    assert( d.contains("anna"))
    assert( d.contains("berta"))
    assert( d.contains("clara"))
    assert( d.contains("dora"))

    val annaId = d("anna")
    val anna = d(annaId)
    assert( "anna" === anna )
  }

  test("datasets"){
    val datasets = new File("src/main/resources/datasets")
    val d = Dictionary.fromDirectory(sc, datasets)
    assert(d.size === 294077)
  }

}
