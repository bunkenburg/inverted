package inverted

import java.io.{File, FileOutputStream, OutputStreamWriter, PrintWriter}

import inverted.InvertedIndex.{CHARSET, DocId, WordId}
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

object InvertedIndex {

  val conf: SparkConf = new SparkConf().setMaster("local[4]").setAppName("Inverted")

  val sc: SparkContext = new SparkContext(conf)

  /** It seems the data files are in this encoding, so let's do everything in this encoding. */
  val CHARSET = "Cp1252"

  type Word = String
  type WordId = Long
  type DocId = Int

  def main(args: Array[String]) {

    //0. make and write the dictionary (Word, WordId).
    val datasets = new File("src/main/resources/datasets")
    val dictionary: Dictionary = Dictionary.fromDirectory(sc, datasets)
    dictionary.write("target/dictionary.txt")

    //1. make and write index (WordId, DocId)
    val dict: Dictionary = Dictionary.read(sc, "target/dictionary.txt")
    val index: Index = Index.fromDirectory(sc, dict, datasets)
    index.write("target/index.txt")

    //4. make and write inverted index
    index.writeInverted("target/inverted.txt")

    sc.stop()
  }

}
