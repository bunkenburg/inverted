package inverted

import java.io.{File, FileOutputStream, OutputStreamWriter, PrintWriter}

import com.databricks.spark.csv.util.TextFile
import inverted.Dictionary.split
import inverted.InvertedIndex.{CHARSET, DocId, Word, WordId}
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

object Index {
  import InvertedIndex.CHARSET

  /** First makes a dictionary for all words in all data files in the directory,
    * and then makes an index for them. */
  def fromDirectory(sc: SparkContext, directory: File): Index = {
    val dictionary = Dictionary.fromDirectory(sc, directory)
    fromDirectory(sc, dictionary, directory)
  }

  /** Makes an index (WordId, DocId) for all words in all data files in the directory,
    * taking word Ids from the dictionary. */
  def fromDirectory(sc: SparkContext, dictionary: Dictionary, directory: File): Index = {

    def wordsFromFile(file: File): RDD[Word] = {
      val lines: RDD[String] = TextFile.withCharset(sc, file.getPath, CHARSET)
      val words: RDD[Word] = lines flatMap split
      words.distinct()
    }

    val files = directory.listFiles()

    val rs: Seq[RDD[(WordId,DocId)]] = files.map{ file =>
      val docId: Int = file.getName.toInt
      val words: RDD[Word] = wordsFromFile(file)
      val wordIds: RDD[WordId] = words map dictionary.apply     //a long time here
      val pairs: RDD[(WordId,DocId)] = wordIds.map( id => (id,docId) )
      pairs
    }

    val index: RDD[(WordId,DocId)] = sc.union(rs)
    new Index(dictionary, index)
  }

}

class Index (
              val dictionary: Dictionary,
              private val rdd: RDD[(WordId,DocId)]
            ){

  private lazy val inverse: RDD[(DocId,WordId)] = rdd.map( _.swap )

  /** Writes the index to output file */
  def write(path: String) = {

    val file = new File(path)
    val os = new FileOutputStream(file)
    val writer = new PrintWriter(new OutputStreamWriter(os, CHARSET))

    def format(p: (WordId,DocId)): String = {
      val wordId = p._1
      val docId = p._2
      s"$wordId $docId"
    }

    def write(p: (WordId,DocId)): Unit = writer.println(format(p))

    // unsorted
    rdd.collect.foreach(write)

    // sort by word id:
    //rdd.sortBy{ case(wordId,docId) => wordId }.collect.foreach(write)

    // sort by doc id:
    // rdd.sortBy{ case(wordId,docId) => docId }.collect.foreach(write)

    writer.close()
  }

  /** Writes the inverted index to output file */
  def writeInverted(path: String) = {

    val file = new File(path)
    val os = new FileOutputStream(file)
    val writer = new PrintWriter(new OutputStreamWriter(os, CHARSET))

    def format(p: (WordId,Iterable[DocId])): String = {
      val wordId = p._1
      val files = p._2.toSeq.sorted
      s"($wordId, ${files.mkString("[", ", ", "]")})"
    }

    def write(p: (WordId,Iterable[DocId])) = writer.println(format(p))

    val r = rdd.groupByKey()

    //unsorted
    //r.collect.foreach(write)

    //sorted
    r.sortBy( _._1 ).collect.foreach(write)

    writer.close()
  }

  /** Which documents does a word appear in? */
  def documents(id: WordId): Seq[DocId] = rdd.lookup(id)

  /** Which documents does a word appear in? */
  def documents(word: Word): Seq[DocId] = rdd.lookup(dictionary(word))

  /** Which words appear in a document? */
  def words(docId: DocId): Seq[WordId] = inverse.lookup(docId)

}