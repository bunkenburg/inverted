package inverted

import java.io.{File, FileOutputStream, OutputStreamWriter, PrintWriter}

import com.databricks.spark.csv.util.TextFile
import InvertedIndex.{CHARSET, Word, WordId}
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

object Dictionary {

  /** Makes a dictionary from all data files in a directory.
    * @param directory The directory of data files.
    *                  Data files should be text in encoding InvertedIndex.CHARSET.
    * */
  def fromDirectory(sc: SparkContext, directory: File): Dictionary = {

    def wordsFromFile(file: File): RDD[Word] = {
      val lines: RDD[String] = TextFile.withCharset(sc, file.getPath, CHARSET)
      val words: RDD[Word] = lines flatMap split
      words.distinct()
    }

    val files = directory.listFiles()
    val rs: Seq[RDD[Word]] = files map wordsFromFile
    val words: RDD[Word] = sc.union(rs)
    val ds = words.distinct().zipWithUniqueId()
    val dm = ds.collectAsMap()
    new Dictionary(dm)
  }

  /** Gets the words in a line.
    * Here, just follows spec: words are separated by one or more spaces.
    * But the words that come out of that are quite dirty.
    * Maybe better to filter non-alphabetic characters?
    * */
  def split(line: String): Array[String] = line.split("[ ]+")

  /** Reads dictionary from file */
  def read(sc: SparkContext, path: String): Dictionary = {

    /** A dictionary line is a word (without space), a space, and an integer. */
    def split(line: String): (Word,WordId) = {
      val ss = line.split(" ")
      val word = ss(0)
      val id = ss(1).toLong
      (word, id)
    }

    val lines = TextFile.withCharset(sc, path, CHARSET)
    val ds = lines map split
    val dm = ds.collectAsMap()
    new Dictionary(dm)
  }

}

/** Dictionary: a map from words to word id.
  *
  * Encapsulates a Map : Word -> WordId
  * but really it's a bijection which is not reflected in code.
  *
  * The map may be large: Nearly 300 000 entries.
  *
  * Not sure whether the map in memory can be avoided.
 */
class Dictionary(private val dm: scala.collection.Map[Word,WordId] ) extends Serializable {

  /** inverse: map from words to their IDs */
  private lazy val inverse = dm.map(_.swap)

  /** Gets the ID for a word. */
  def apply(word: Word): WordId = dm(word)

  /** Gets the word for an ID. */
  def apply(id: WordId): Word = inverse(id)

  /** Looks up a word in the dictionary. */
  def contains(word: Word): Boolean = dm.contains(word)

  /** Looks up a word in the dictionary. */
  def size: Int = dm.size

  /** Writes the dictionary to output file */
  def write(path: String) = {

    val file = new File(path)
    val os = new FileOutputStream(file)
    val writer = new PrintWriter(new OutputStreamWriter(os, CHARSET))

    def format(p: (Word,WordId)): String = p._1 + " " + p._2
    def write( p: (Word,WordId) ) = writer.println(format(p))

    // write Map[Word,WordId] unsorted
    dm.foreach{ write }

    // write sorted by Word
    //dm.toList.sortBy{ case(word,id) => word }.foreach(write)

    // write sorted by WordId
    //dm.toList.sortBy{ case(word,id) => id }.foreach(write)

    writer.close()
  }

}