# read me

spec:

* doc/DNA-Coding-Challenge.pdf
* doc/challenge.pdf

documentation:

* doc/notes.md
* doc/api.md

How to run it:

    sbt run
    
or in you IDE.