# technical notes

# language and framework

I solved the coding challenge using Apache Spark rather then Scio as specified 
because I read the DNA-Coding-Challenge.pdf, then I read challenge.pdf, and there
I saw "You may use Hadoop or Spark.", so I started with Scala and Spark.

By the time I went back to the first PDF, I already had a lot done.

Anyway, I have done very little in Spark (the exercises in the five Coursera Scala courses),
so of Spark I know little and of Scio nothing, so I wouldn't have been very efficient.

I used Scala rather than Java because I have recently learnt it and I like it very much.
It finally brings many of the advantages of FP that I studied and researched so many
years ago to an industrially useful language.

## program design

I started with a copy of a an exercise from a Coursera course and updated the versions
of Scala, sbt, Spark, and scalatest.

In Spark programming I used RDD rather than the more modern Datasets and Dataframes simply
for the reason that I had used them slightly more in the Coursera exercises. I suppose that
with Datasets and Dataframes the performance will be similar, whereas the API convenience
is better.

I added some test classes using the scalatest framework because having tests makes programming
so much easier. Any change is validated by recompilation and the test cases exercising all
the methods, so the programmer has a lot more confidence in correctness.

I generally wrote two tests: one using a small dataset of only two tiny files, and one
using the real dataset. The small test verifies correctness whereas the other one verifies
that the performance is okay. Executing the main program InvertedIndex.main takes about
two minutes on my normal desktop computer.

During development, first I concentrated on algorithm and on getting Spark to work,
and later, focused more on object-oriented programming. This second step made me make
separated classes for Dictionary, Index, and InvertedIndex. Finally, I revised the design
from the point of view of the client programmer (the one that will use my classes). 
See api.md. This last step made me delete the class InvertedIndex (its only method went
to Index), and made me make the dictionary be a field in Index.

