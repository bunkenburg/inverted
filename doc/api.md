# Scala API

Design and implement an API in Scala to access the output produced by the previous
exercise.

## Dictionary

A dictionary is a bijection between words and IDs. 

We want to use a smaller faster datatype for identifying words than String itself.
It can be smaller since we don't need to identify all possible Strings, but rather
only those that appear in a certain collection of source files.

As far API is concerned, we need a way to construct a dictionary from a collection
of source files. For example, something like:

    val dictionary = Dictionary.fromDirectory("src/main/resources/datasets")
    
Once we have a dictionary, it may be useful to write it to file and read it from file,
so that we can re-use it without having to construct it again. Something like:

    dictionary.write("target/dictionary.txt")
    val d = Dictionary.read("target/dictionary.txt")
    
What can we do with a dictionary? We can use it to convert a word to its ID and the reverse.
Something like:

    val id = dictionary("anna")
    val word = dictionary(id)
    
Maybe using the same method name "apply" (which is Scala becomes invisible) can be discussed.
It turns out very short, but also could be confusing, and the distinction is only by type.

## Index

The index records which words appear in which documents. 
It's a relation between WordId and DocId.
We can model it as a set of pairs (WordId,DocId).

It's called "index" like the index at the end of a book that lists some of the words
that appear in the book. This metaphor leads us to see an index as a mapping from
document to word.

Note that an index depends on a dictionary, in order to map between words and word IDs.

As far API is concerned, we need a way to construct an index from a collection
of source files. For example, something like:

    val index = Index.fromDirectory(dictionary, "src/main/resources/datasets")

Once we have an index, it may be useful to write it to file and read it from file,
so that we can re-use it without having to construct it again. Something like:

    index.write("target/index.txt")
    val i = Index.read("target/index.txt")      //not implemented

What can we do with an index? We can find out which documents a certain word appears in.
Something like:

    val wordId = dictionary("anna")
    val documents: Iterable[DocId] = index.documents(wordId)

We could also implement the reverse: finding out which words appear in a given document.
This is the direction that gives rise to "index".
Something like:

    val wordIds: Iterable[WordId] = index.words(docId)

Maybe it would be quite convenient to find out which documents a word appears in without
having to find its word ID first. Something like:

    val documents = index.documents("anna")

In that case, we'd have to ensure that the index knows which dictionary it has been built with.
Let's hide the dictionary in the index. Something like:

    val index = Index.fromDirectory("src/main/resources/datasets")
    val documents = index.documents("anna")
    

## InvertedIndex 

The inverted index is the same as the index, except that we see it as a relation from
word to document. 

We do need because we have to implement the spec that ask for the inverted index written
to file in a certain form. But maybe we don't need it as a separate class and concept.